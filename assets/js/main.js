var $userLogged = localStorage.getItem("userLogged");
var $user = ($userLogged === "" ? null : JSON.parse($userLogged));
var $chiPhiChienDich = 0,
  $hoaHong = 0,
  $typeHoaHong = 0,
  $soTienNhanDuoc = 0;

function getSoTienNhanDuoc($models) {
  $chiPhiChienDich = $models.chi_phi_chien_dich;
  $hoaHong = $models.Capped;
  $typeHoaHong = $models.type_hoa_hong;
  $soTienNhanDuoc = 0;

  if ($typeHoaHong === "Phần trăm")
    $soTienNhanDuoc = ($hoaHong * $chiPhiChienDich) / 100;
  else $soTienNhanDuoc = $hoaHong;
}

// register
var noiDungThongBao = document.getElementById("noi-dung-thong-bao");
var DialogIconedDanger = new bootstrap.Modal(
  document.getElementById("DialogIconedDanger"),
  {
    keyboard: false,
  }
);

// $(document).on('ready', function (e){
//   e.preventDefault()
//   let count;
//   count = parseInt(Cookies.get('counter'));
//   if(count){
//     Cookies.set('counter', count + 1);
//   }
//   else{
//     Cookies.set('counter', 1);
//   }
//   console.log(count)
// })

$(document).ready(function () {
  $.ajax({
    url: "https://app.vietlixi.com/api/service/get-contact",
    data: {},
    dataType: "json",
    type: "post",
    beforeSend: function () {
      $("#row-content-app-blog").html("");
    },
    success: function (data) {
      $("#lien_he_admin").html(`
        <div class="float-icon-hotline">
            <ul class ="left-icon hotline">
                <li class="hotline_float_icon"><a target="_blank" rel="nofollow" id="messengerButton" href="`+ data.zalo +`"><i class="fa fa-zalo animated infinite tada"></i><span>Zalo</span></a></li>
                <li class="hotline_float_icon"><a target="_blank" rel="nofollow" id="messengerButton" href="`+ data.facebook +`"><i class="fa fa-messenger animated infinite tada"></i><span>Facebook</span></a></li>
            </ul>
    </div>
        `)
    },
    error: function (r1, r2) {
      noiDungThongBao.innerText = r1.responseJSON.message;
      DialogIconedDanger.show();
    },
  });

});
// Login
$("#btn-login").click(function (e) {
  e.preventDefault();
  $.ajax({
    url: "https://app.vietlixi.com/api/store-api/login-app",
    data: $("#form-login").serializeArray(),
    dataType: "json",
    type: "post",
    beforeSend: function () {},
    success: function (data) {

      localStorage.setItem("userLogged", JSON.stringify(data.user));
      if (localStorage.getItem("currentUrl")) {
        var link = localStorage.getItem("currentUrl");
        localStorage.removeItem("currentUrl");
        window.location = link;
      } else {
        window.location = "https://vietlixi.com/";
      }
    },
    error: function (r1, r2) {
      noiDungThongBao.innerText = r1.responseJSON.message;
      DialogIconedDanger.show();
    },
  });
});

if($("#form-auto-login").length > 0){
  $.ajax({
    url: "https://app.vietlixi.com/api/store-api/login-app",
    data: $("#form-auto-login").serializeArray(),
    dataType: "json",
    type: "post",
    beforeSend: function () {},
    success: function (data) {
      localStorage.setItem("userLogged", JSON.stringify(data.user));
      if (localStorage.getItem("currentUrl")) {
        var link = localStorage.getItem("currentUrl");
        localStorage.removeItem("currentUrl");
        window.location = link;
      } else {
        window.location = "https://vietlixi.com/";
      }
    },
    error: function (r1, r2) {
      noiDungThongBao.innerText = r1.responseJSON.message;
      DialogIconedDanger.show();
    },
  });
}

// Index
if ($("#body-index").length > 0) {
  $.ajax({
    url: "https://app.vietlixi.com/api/service/init-index",
    data: {
      user: $user,
    },
    dataType: "json",
    type: "post",
    beforeSend: function () {
      $(
        "#list-kiem-tien, #block-hoan-tien-shopback .transactions, #block-dich-vu-khac ul"
      ).html("");
    },
    success: function (data) {
      $.each(data.kiemTien, function (k, obj) {
        $("#list-kiem-tien").append(
          `<li class="splide__slide">
                            <a href="#" data-value="` +
            obj.id +
            `" data-type="Kiếm tiền" class="btn-xem-chi-tiet">
                                <div class="blog-card blog-kiem-tien">
                                    <img src="` +
            obj.image +
            `" alt="` +
            obj.title +
            `" class="imaged-blog w-100">
                                    <div class="text">
                                        <h4 class="title">` +
            obj.title +
            `</h4>
                                        <h3 id="pricing-offer" class="text-center">+ <span  class="text-danger font-h3-big">` +
            Number(obj.chi_phi_chien_dich).toLocaleString("vi") +
            `</span> VNĐ</h3>
                                    </div>
                                </div>
                            </a>
                        </li>`
        );
      });

      $.each(data.dichVu, function (k, obj) {
        $("#block-dich-vu-khac ul").append(
          `<li class="splide__slide">
                            <a href="#" data-value="` +
            obj.id +
            `" data-type="Dịch vụ" class="btn-xem-chi-tiet">
                                <div class="blog-card">
                                    <img src="` +
            obj.image +
            `" alt="` +
            obj.name +
            `" class="imaged w-100">
                                    <div class="text">
                                        <h4 class="title">` +
            obj.name +
            `</h4>
                                    </div>
                                </div>
                            </a>
                        </li>`
        );
      });

      $.each(data.shopbackHoanTien, function (k, obj) {
        $("#block-hoan-tien-shopback .transactions").append(
          `<a href="#" class="item btn-xem-chi-tiet" data-value="` +
            obj.id +
            `" data-type="Hoàn tiền Shopback" class="btn-xem-chi-tiet">
                                <div class="detail">
                                    <img src="` +
            obj.image +
            `" alt="` +
            obj.name +
            `" class="image-block imaged w48">
                                    <div>
                                        <strong>` +
            obj.name +
            `</strong>
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="price text-danger">Hoàn tiền ` +
            obj.ti_le_hoan_tien +
            `%</div>
                                </div>
                            </a>`
        );
      });

      // Multiple Carousel
      document.querySelectorAll(".carousel-multiple").forEach((carousel) =>
        new Splide(carousel, {
          perPage: 4,
          rewind: true,
          type: "loop",
          gap: 16,
          padding: 16,
          arrows: false,
          pagination: false,
          breakpoints: {
            768: {
              perPage: 2,
            },
            991: {
              perPage: 3,
            },
          },
        }).mount()
      );

      localStorage.setItem(
        "userLogged",
        data.user === null ? "" : JSON.stringify(data.user)
      );

      if ($user === null) {
        $(".region-welcome").addClass('hidden');
        $("#top_welcome").removeClass('hidden').html(
          `<p class="text-success">
                          <ion-icon name="notifications-outline"></ion-icon>
                            <a href="https://vietlixi.com/app-register.html" title="Đăng ký tài khoản">Đăng ký</a> 
                            / <a href="https://vietlixi.com/app-login.html" title="Đăng nhập Việt Lì xì">Đăng nhập</a> 
                            để sử dụng nhiều hơn các tiện ích của ứng dụng nhé!
                    </p>`
        );
      } else {
        $("#top_welcome").addClass('hidden');
        $('.region-welcome').removeClass('hidden');
        $("#block-welcome").html(
          `
              <!-- Wallet Footer -->
              <div class="wallet-footer">
                  <div class="item">
                      <a href="app-cards.html">
                          <div class="icon-wrapper bg-success">
                              <ion-icon name="wallet-outline"></ion-icon>
                          </div>
                          <strong>Ví</strong>
                      </a>
                  </div>
                  <div class="item">
                      <a href="app-blog.html">
                          <div class="icon-wrapper bg-danger">
                              <ion-icon name="bag-handle-outline"></ion-icon>
                          </div>
                          <strong>Kiếm tiền</strong>
                      </a>
                  </div>
                  <div class="item">
                      <a href="app-transactions-history.html">
                          <div class="icon-wrapper">
                              <ion-icon name="receipt-outline"></ion-icon>
                          </div>
                          <strong>Giao dịch</strong>
                      </a>
                  </div>
              </div>
              <!-- * Wallet Footer -->
            `
        );
      }
      var $str = '';

      if(data.topBanner.length > 0){
        $str = '';
        $.each(data.topBanner, function (k, obj){
          var $bodyBanner = `<div class="card-body">
                                    <h5 class="card-title">`+obj.title+`</h5>
                                </div>`;
          if(obj.title.trim !== ''){
            $str += `<li class="splide__slide">
                            <div class="card">
                                <a target="_blank" href="`+obj.link+`" title="`+obj.title+`"><img src="https://app.vietlixi.com/api/images/`+obj.image+`" class="card-img-top" alt="`+obj.title+`"></a>
                            </div>
                        </li>`
          }
        })
        $("#top_banner").html(`
          <div class="section wallet-card-section pt-1">
            <div id="top-banner-welcome">
              <div class="carousel-single splide">
                <div class="splide__track">
                    <ul class="splide__list">
                        `+$str+`
                    </ul>
                </div>
              </div>
            </div>
          </div>
        `);
      }

      if(data.middleBanner.length > 0){
        $str = '';
        $.each(data.middleBanner, function (k, obj){
          if(obj.title.trim !== ''){
            $str += `<li class="splide__slide">
                            <div class="card">
                                <a target="_blank" href="`+obj.link+`" title="`+obj.title+`"><img src="https://app.vietlixi.com/api/images/`+obj.image+`" class="card-img-top" alt="`+obj.title+`"></a>
                            </div>
                        </li>`
          }
        })
        $("#block-middle-banner").html(
            `
              <div class="section mg-3">
                <div id="middle-banner-welcome">
                  <div class="carousel-single splide">
                    <div class="splide__track">
                        <ul class="splide__list">
                            `+$str+`
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
            `
        );
      }

      if(data.bottomBanner.length > 0){
        $str = '';
        $.each(data.bottomBanner, function (k, obj){
          if(obj.title.trim !== ''){
            $str += `<li class="splide__slide">
                            <div class="card">
                                <a target="_blank" href="`+obj.link+`" title="`+obj.title+`"><img src="https://app.vietlixi.com/api/images/`+obj.image+`" class="card-img-top" alt="`+obj.title+`"></a>
                            </div>
                        </li>`
          }
        })
        $("#block-bottom-banner").html(
            `
              <div class="section mg-3">
                <div id="middle-banner-welcome">
                  <div class="carousel-single splide">
                    <div class="splide__track">
                        <ul class="splide__list">
                            `+$str+`
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
            `
        );
      }

      document.querySelectorAll('.carousel-single').forEach(carousel => new Splide(carousel, {
        perPage: 1,
        rewind: true,
        type: "loop",
        gap: 10,
        padding: 0,
        arrows: true,
        pagination: true,
      }).mount());

    },
    error: function (r1, r2) {
      noiDungThongBao.innerText = r1.responseJSON.message;
      DialogIconedDanger.show();
    },
  });
}

if ($("#register-body").length == 0 && $("#login-body").length == 0) {
  if ($user != null) {
    $(".profileBox .in strong").html($user === null ? "Khách" : $user.ho_ten);
    $(".profileBox .in .text-muted").html(
      $user === null ? "" : $user.dien_thoai
    );
    $(".sidebar-balance .listview-title, .balance .title").html("Ví của bạn");
    // $(".sidebar-balance .in h1.amount, .balance h1.total")
    // .html($user.total === null ? 0 : parseInt($user.total)
    //     .toLocaleString('vi'));
  }
}

if ($("#login-body").length > 0) {
  if ($user != null) {
    window.location = "https://vietlixi.com/";
  }
}

if ($("#login-register").length > 0) {
  if ($user != null) {
    window.location = "https://vietlixi.com/";
  }
}

// Xem chi tieets dichj vuj, hoan tien, kiemtien
$(document).on("click", ".btn-xem-chi-tiet", function (e) {
  e.preventDefault();
  $.ajax({
    url: "https://app.vietlixi.com/api/service/detail",
    data: {
      value: $(this).attr("data-value"),
      type: $(this).attr("data-type"),
    },
    dataType: "json",
    type: "post",
    beforeSend: function () {},
    success: function (data) {
      if(data.router !== ''){
        localStorage.setItem("models", JSON.stringify(data.models));
        localStorage.setItem("titlePage", JSON.stringify(data.title));
        window.location = "https://vietlixi.com/" + data.router;
      }else{
        noiDungThongBao.innerText = 'Chiến dịch này không khả dụng';
        DialogIconedDanger.show();
      }
    },
    error: function (r1, r2) {
      noiDungThongBao.innerText = r1.responseJSON.message;
      DialogIconedDanger.show();
    },
  });
});

if ($("#body-blog-post").length > 0) {
  var $models = JSON.parse(localStorage.getItem("models"))[0];
  console.log($models)

  $("h1.title-offer, title, .pageTitle").text($models.title);
  $("#pricing-offer-mo-ta span").text(Number($models.chi_phi_chien_dich).toLocaleString("vi"));
  $("#image-offer").html(
    `<img class="img-fluid" alt="` +
      $models.title +
      `" src="` +
      $models.image +
      `"/>`
  );
  if ($models.preview_offer) {
    $("#inner-block-preview-offer").html($models.preview_offer);
  }
  $("#inner-block-detail-offer").html($models.conversion_requirements);
  $("#appCapsule .block-click-to-registation").html(
    `<a href="#" class="btn btn-block btn-primary btn-dang-ky-ngay" data-value="` +
      $models.id +
      `">
                    <ion-icon name="share-outline"></ion-icon> Đăng ký ngay
                </a>`
  );

  $(document).on("click", ".btn-block.btn-view-guide", function (e) {
    e.preventDefault();
    if ($("#block-detail-offer").hasClass("hidden"))
      $("#block-detail-offer").removeClass("hidden");
    else $("#block-detail-offer").addClass("hidden");
  });
  // Load các chiến dịch khác
  if($("#block-chien-dich-khac").length > 0){
    $.ajax({
      url: "https://app.vietlixi.com/api/service/get-other-models",
      data: {
        model: $models.id,
      },
      dataType: "json",
      type: "post",
      beforeSend: function () {
      },
      success: function (data) {
        $(data.models).each(function (k, obj) {
          $("#list-chien-dich-khac").append(
              `<li class="splide__slide">
                            <a href="#" data-value="` +
              obj.id +
              `" data-type="Kiếm tiền" class="btn-xem-chi-tiet">
                                <div class="blog-card blog-kiem-tien">
                                    <img src="` +
              obj.image +
              `" alt="` +
              obj.title +
              `" class="imaged-blog w-100">
                                    <div class="text">
                                        <h4 class="title">` +
              obj.title +
              `</h4>
                                        <h3 id="pricing-offer" class="text-center">+ <span  class="text-danger font-h3-big">` +
              Number(obj.chi_phi_chien_dich).toLocaleString("vi") +
              `</span> VNĐ</h3>
                                    </div>
                                </div>
                            </a>
                        </li>`
          );
        });

        document.querySelectorAll(".carousel-multiple").forEach((carousel) =>
            new Splide(carousel, {
              perPage: 4,
              rewind: true,
              type: "loop",
              gap: 16,
              padding: 16,
              arrows: false,
              pagination: false,
              breakpoints: {
                768: {
                  perPage: 2,
                },
                991: {
                  perPage: 3,
                },
              },
            }).mount()
        );
      },
      error: function (r1, r2) {
        noiDungThongBao.innerText = r1.responseJSON.message;
        DialogIconedDanger.show();
        $(".btn-dang-ky-ngay").addClass("hidden");
      },
    });
  }
  // Load các chiến dịch vay tiền
  if($("#block-chien-dich-vay-tien").length > 0){
    $.ajax({
      url: "https://app.vietlixi.com/api/service/get-other-models-vay-tien",
      data: {
        model: $models.id,
      },
      dataType: "json",
      type: "post",
      beforeSend: function () {
      },
      success: function (data) {
        $(data.models).each(function (k, obj) {
          $("#list-chien-dich-vay-tien").append(
              `<li class="splide__slide">
                            <a href="#" data-value="` +
              obj.id +
              `" data-type="Vay tiền" class="btn-xem-chi-tiet">
                                <div class="blog-card blog-kiem-tien">
                                    <img src="` +
              obj.image +
              `" alt="` +
              obj.title +
              `" class="imaged-blog w-100">
                                    <div class="text">
                                        <h4 class="title">` +
              obj.title +
              `</h4>
                                        <h3 id="pricing-offer" class="text-center">+ <span  class="text-danger font-h3-big">` +
              Number(obj.chi_phi_chien_dich).toLocaleString("vi") +
              `</span> VNĐ</h3>
                                    </div>
                                </div>
                            </a>
                        </li>`
          );
        });

        document.querySelectorAll(".carousel-multiple").forEach((carousel) =>
            new Splide(carousel, {
              perPage: 4,
              rewind: true,
              type: "loop",
              gap: 16,
              padding: 16,
              arrows: false,
              pagination: false,
              breakpoints: {
                768: {
                  perPage: 2,
                },
                991: {
                  perPage: 3,
                },
              },
            }).mount()
        );
      },
      error: function (r1, r2) {
        noiDungThongBao.innerText = r1.responseJSON.message;
        DialogIconedDanger.show();
        $(".btn-dang-ky-ngay").addClass("hidden");
      },
    });
  }
}

$(document).on("click", ".btn-dang-ky-ngay", function (e) {
  e.preventDefault();
  // Đăng ký chiến dịch
  $.ajax({
    url: "https://app.vietlixi.com/api/service/dang-ky-chien-dich",
    data: {
      model: $(this).attr("data-value"),
      user: $user,
    },
    dataType: "json",
    type: "post",
    beforeSend: function () {
      $("#modal-icon")
        .removeClass("text-sucess")
        .addClass("text-danger")
        .html('<ion-icon name="close-circle"></ion-icon>');
    },
    success: function (data) {
      if (data.success === "failed") {
        localStorage.setItem("currentUrl", window.location.href);
        noiDungThongBao.innerHTML = data.content;
        DialogIconedDanger.show();
        var $time = data.time;
        setInterval(function () {
          $("#thoi-gian").text($time);
          $time--;
        }, 1000);
        setTimeout(function () {
          window.location = "https://vietlixi.com/app-login.html";
        }, 10000);
      } else {
        localStorage.setItem("userLogged", JSON.stringify(data.user));
        window.open(data.url, '_blank');
        noiDungThongBao.innerText = data.content;
        $("#modal-icon")
          .removeClass("text-danger")
          .addClass("text-success")
          .html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
        DialogIconedDanger.show();
      }
    },
    error: function (r1, r2) {
      noiDungThongBao.innerText = r1.responseJSON.message;
      DialogIconedDanger.show();
    },
  });
});

$("#btn-register").click(function (e) {
  e.preventDefault();
  $.ajax({
    url: "https://app.vietlixi.com/api/store-api/register",
    data: $("#form-dang-ky").serializeArray(),
    dataType: "json",
    type: "post",
    beforeSend: function () {
    },
    success: function (data) {
      console.log(data.user)

      localStorage.setItem("userLogged", JSON.stringify(data.user));
      localStorage.setItem("thongBao", data.content);
      window.location = "https://vietlixi.com/";
    },
    error: function (r1, r2) {
      noiDungThongBao.innerText = r1.responseJSON.message;
      DialogIconedDanger.show();
    },
  });
});

if (document.querySelector(".offcanvas") === null) {
  // Doesn't exist.
} else {
  var elCookiesBox = new bootstrap.Offcanvas(
    document.getElementById("cookiesbox")
  );
  var CookiesStatus = localStorage.getItem("FinappCookiesStatus");
  function CookiesBox(time) {
    if (CookiesStatus === "1" || CookiesStatus === 1) {
      // Cookies already accepted.
    } else {
      if (time) {
        setTimeout(() => {
          elCookiesBox.toggle();
        }, time);
      } else {
        elCookiesBox.toggle();
      }
    }
  }
  document.querySelectorAll(".accept-cookies").forEach(function (el) {
    el.addEventListener("click", function () {
      localStorage.setItem("FinappCookiesStatus", "1");
    });
  });
}

//-----------------------------------------------------------------------
